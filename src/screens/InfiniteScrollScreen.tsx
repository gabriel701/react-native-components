import React, { useContext, useState } from 'react';
import { FlatList, StyleSheet, View, ActivityIndicator } from 'react-native';
import { FadeInImage } from '../components/FadeInImage';
import { HeaderTitle } from '../components/HeaderTitle';
import { ThemeContext } from '../context/ThemeContext/ThemeContext';

export const InfiniteScrollScreen = () => {
  const [numbers, setNumbers] = useState([0, 1, 2, 3, 4, 5]);

  const {
    theme: { colors },
  } = useContext(ThemeContext);
  const renderItem = (item: number) => {
    return (
      <FadeInImage
        uri={`https://picsum.photos/id/${item}/500/400`}
        style={{
          flex: 1,
          height: 500,
          width: '95%',
          borderRadius: 30,
          marginVertical: 10,
          marginHorizontal: 10,
          alignItems: 'center',
        }}
      />
      // <Image
      //   source={{ uri: `https://picsum.photos/id/${item}/500/400` }}
      //   style={{ height: 500, width: 400 }}
      // />
    );
  };

  const loadMore = () => {
    console.log('loadMore');
    const newArray: number[] = [];
    for (let i = 0; i < 10; i++) {
      newArray[i] = numbers.length + i;
    }

    setTimeout(() => {
      setNumbers([...numbers, ...newArray]);
    }, 1500);
  };

  return (
    <View style={{ flex: 1 }}>
      <FlatList
        data={numbers}
        keyExtractor={item => item.toString()}
        renderItem={({ item }) => renderItem(item)}
        ListHeaderComponent={
          <View style={{ marginHorizontal: 20 }}>
            <HeaderTitle title="Inifine Scroll" />
          </View>
        }
        onEndReached={() => loadMore()}
        onEndReachedThreshold={0.5}
        ListFooterComponent={() => (
          <View
            style={{
              height: 150,
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <ActivityIndicator size={25} color={colors.primary} />
          </View>
        )}
      />
    </View>
  );
};
