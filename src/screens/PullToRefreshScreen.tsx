import React, { useContext, useState } from 'react';
import { View, ScrollView, RefreshControl } from 'react-native';
import { HeaderTitle } from '../components/HeaderTitle';
import { stylesApp } from '../theme/appTheme';
import { ThemeContext } from '../context/ThemeContext/ThemeContext';

export const PullToRefreshScreen = () => {
  const [refresing, setRefresing] = useState(false);

  const {
    theme: { colors, dividerColor, dark },
  } = useContext(ThemeContext);

  const onRefresh = () => {
    setRefresing(true);
    setTimeout(() => {
      setRefresing(false);
    }, 1500);
  };

  return (
    <ScrollView
      style={stylesApp.container}
      refreshControl={
        <RefreshControl
          refreshing={refresing}
          onRefresh={onRefresh}
          progressViewOffset={10}
          progressBackgroundColor={dividerColor}
          colors={[colors.text]}
          tintColor={dark?'white':'black'}
        />
      }>
      <View style={stylesApp.globalMargin}>
        <HeaderTitle title="Pull to refresh" />
      </View>
    </ScrollView>
  );
};
