import React, { useContext } from 'react';
import { View, StyleSheet, Animated, Button } from 'react-native';
import { useAnimation } from '../hooks/useAnimation';
import { ThemeContext } from '../context/ThemeContext/ThemeContext';

export const Animated101Screen = () => {
  const { opacity, position, fadeIn, fadeOut, startMovingPosition } =
    useAnimation();

  const {
    theme: { colors },
  } = useContext(ThemeContext);

  return (
    <View style={styles.container}>
      <Animated.View
        style={{
          ...styles.purpleBox,
          backgroundColor: colors.primary,
          opacity,
          marginBottom: 20,
          transform: [
            {
              translateY: position,
            },
          ],
        }}
      />
      <View style={{ marginHorizontal: 10, flexDirection: 'row' }}>
        <Button
          title="FadeIn"
          onPress={() => {
            fadeIn(opacity);
            startMovingPosition(-100);
          }}
          color={colors.primary}
        />
        <View style={{ marginHorizontal: 20 }} />
        <Button
          title="FadeOut"
          onPress={() => {
            fadeOut(opacity);
            startMovingPosition(-100);
          }}
          color={colors.primary}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  purpleBox: {
    backgroundColor: '#5856D6',
    width: 150,
    height: 150,
  },
});
