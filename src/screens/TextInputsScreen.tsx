import React, { useContext } from 'react';
import {
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { HeaderTitle } from '../components/HeaderTitle';
import { stylesApp } from '../theme/appTheme';
import { useForm } from '../hooks/useForm';
import { CustomSwitch } from '../components/CustomSwitch';
import { ThemeContext } from '../context/ThemeContext/ThemeContext';

export const TextInputsScreen = () => {
  const { form, onChange, isSuscribe } = useForm({
    name: '',
    email: '',
    phone: '',
    isSuscribe: false,
  });
  const { theme :{colors, dividerColor}} = useContext(ThemeContext);

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
      <ScrollView>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <View style={stylesApp.globalMargin}>
            <HeaderTitle title="Text Inputs screen" />

            <TextInput
              style={{
                ...styles.inputStyle,
                borderColor: colors.text,
                color: colors.text,
              }}
              placeholder="Ingrese su nombre"
              autoCorrect={false}
              autoCapitalize={'words'}
              onChangeText={value => onChange(value, 'name')}
              keyboardAppearance="dark"
              placeholderTextColor={dividerColor}
            />
            <TextInput
              style={{
                ...styles.inputStyle,
                borderColor: colors.text,
                color: colors.text,
              }}
              placeholder="Ingrese su emai"
              autoCorrect={false}
              autoCapitalize={'none'}
              onChangeText={value => onChange(value, 'email')}
              keyboardType="email-address"
              keyboardAppearance="dark"
              placeholderTextColor={dividerColor}
            />
            <View style={stylesApp.switchRow}>
              <Text style={stylesApp.switchText}>suscribirse</Text>
              <CustomSwitch
                isOn={isSuscribe}
                onChange={value => onChange(value, 'isSuscribe')}
              />
            </View>
            <HeaderTitle title={JSON.stringify(form, null, 3)} />
            <HeaderTitle title={JSON.stringify(form, null, 3)} />
            <TextInput
              style={{
                ...styles.inputStyle,
                borderColor: colors.text,
                color: colors.text,
              }}
              placeholder="Ingrese su telefono"
              autoCorrect={false}
              onChangeText={value => onChange(value, 'phone')}
              keyboardType="phone-pad"
              keyboardAppearance="dark"
              placeholderTextColor={dividerColor}
            />
          </View>
          <View style={{ height: 150 }} />
        </TouchableWithoutFeedback>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  inputStyle: {
    borderWidth: 2,
    height: 50,
    borderColor: 'rgba(0,0,0, 0.5)',
    paddingHorizontal: 10,
    borderRadius: 10,
    marginVertical: 10,
  },
});
