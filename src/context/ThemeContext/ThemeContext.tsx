import React, { useEffect, useReducer } from 'react';
import { createContext } from 'react';
import { Appearance } from 'react-native';

import { AppState } from 'react-native';
import {
  ThemeState,
  themeReducer,
  lightTheme,
  darkTheme,
} from './ThemeReducer';

interface ThemeContextProps {
  theme: ThemeState;
  setDarkTheme: () => {};
  setLightTheme: () => {};
}

export const ThemeContext = createContext({} as ThemeContextProps);

export const ThemeProvider = ({ children }: any) => {
  const [theme, dispatch] = useReducer(
    themeReducer,
    Appearance.getColorScheme() === 'dark' ? darkTheme : lightTheme,
  );

  const setDarkTheme = () => {
    dispatch({ type: 'set_dark_theme' });
    console.log('setDarkTheme');
  };

  const setLightTheme = () => {
    dispatch({ type: 'set_light_theme' });
    console.log('setLightTheme');
  };

  useEffect(() => {
    AppState.addEventListener('change', status => {
      console.log(status);
      if (status === 'active') {
        console.log(Appearance.getColorScheme());
        Appearance.getColorScheme() === 'light'
          ? setLightTheme()
          : setDarkTheme();
      }
    });
  }, []);

  return (
    <ThemeContext.Provider
      value={{
        theme,
        setDarkTheme,
        setLightTheme,
      }}>
      {children}
    </ThemeContext.Provider>
  );
};
