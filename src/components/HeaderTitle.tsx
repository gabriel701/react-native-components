import React, { useContext } from 'react';
import { View, Text } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { stylesApp } from '../theme/appTheme';
import { ThemeContext } from '../context/ThemeContext/ThemeContext';

interface Props {
  title: string;
}

export const HeaderTitle = ({ title }: Props) => {
  const { top } = useSafeAreaInsets();
  const {
    theme: { colors },
  } = useContext(ThemeContext);


  return (
    <View style={{ marginTop: top + 10, marginBottom: 10 }}>
      <Text style={{...stylesApp.title, color: colors.text}}>{title}</Text>
    </View>
  );
};
