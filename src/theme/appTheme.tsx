import { StyleSheet } from 'react-native';

export const stylesApp = StyleSheet.create({
  title: {
    fontSize: 35,
    fontWeight: 'bold',
  },
  globalMargin: {
    marginHorizontal: 10,
  },
  switchText: {
    fontSize: 25,
  },
  switchRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 10,
  },
  container: {
    flex: 1,
  },
});
